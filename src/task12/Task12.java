package task12;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс, который из двумерного массива заполненного случайными числами переносит построчно эти числа в одномерный массив
 *
 * @author Andreeva V.A.
 */
public class Task12 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Количество строк: ");
        int line = scanner.nextInt();
        System.out.print("Количество колонок: ");
        int column = scanner.nextInt();

        int[][] matrix = new int[line][column];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
            }
        }

        for (int i = 0; i < line; i++, System.out.println()) {
            for (int j = 0; j < column; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }

        ArrayList<Integer> massive = new ArrayList<>();
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                massive.add(matrix[i][j]);
            }
        }

        int size = massive.size();
        System.out.println("Одномерный массив: ");
        for (int i = 0; i < size; i++) {
            if (i % column == 0 && i != 0) {
                System.out.print(" ");
            }
            System.out.print(massive.get(i));
        }
    }
}

