package task8;

import java.util.Scanner;

/**
 * Класс, полностью обнуляющий столбец прямоугольной матрицы, который соответствует заданному числу
 *
 * @author Andreeva V.A.
 */
public class Task8 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Количество строк: ");
        int line = scanner.nextInt();
        System.out.print("Количество колонок: ");
        int column = scanner.nextInt();

        if (column == line) {
            System.out.println("Матрица должна быть прямоугольной");
            return;
        }
        int[][] matrix = new int[line][column];
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
            }
        }

        printMatrix(line, column, matrix);
        nullColumn(matrix);
        printMatrix(line, column, matrix);
    }

    /**
     * Выводит матрицу
     *
     * @param line   строки
     * @param column столбцы
     * @param matrix матрица
     */
    private static void printMatrix(int line, int column, int[][] matrix) {
        for (int i = 0; i < line; i++, System.out.println()) {
            for (int j = 0; j < column; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }
    }

    /**
     * Полностью обнуляет столбец прямоугольной матрицы, который соответствует заданному числу
     *
     * @param a матрица
     */
    private static void nullColumn(int[][] a) {
        System.out.print("Число для обнуления: ");
        int number = scanner.nextInt();
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                if (number == a[i][j]) {
                    a[0][j] = a[i][j];
                    for (int k = 0; k < a.length; k++) {
                        a[k][j] = 0;
                    }
                }
            }
        }
    }
}

