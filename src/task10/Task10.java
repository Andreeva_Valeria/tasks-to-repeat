package task10;

import java.util.Scanner;
/**
 * Класс, который проверяет и выводит является ли строка палиндромом или нет
 *
 * @author Andreeva V.A.
 */
public class Task10 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите строку - ");
        String s = scanner.nextLine();
        s = s.replaceAll("[^A-Za-zА-Яа-я0-9]", "");
        if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase())) {
            System.out.println("Строка - Палиндром");
        } else {
            System.out.println("Строка- Не палиндром");
        }
    }
}
