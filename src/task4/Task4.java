package task4;

/**
 * Класс для расчета расстояния до места удара молнии
 *
 * @author Andreeva V.A.
 */
public class Task4 {
    public static void main(String[] args) {
        final double speed = 1234.8;
        final double interval = 6.8;
        double distance = speed / 3600 * interval;
        System.out.println("Расстояние составляет " + String.format("%.2f", distance) + " км");
    }
}

