package task3;

import java.util.Scanner;

/**
 * Класс для перевода рублей в евро по заданному курсу
 *
 * @author Andreeva V.A.
 */
public class Task3 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите сумму в рублях - ");
        double ruble = scanner.nextDouble();
        System.out.println("Введите курс - ");
        double course = scanner.nextDouble();

        rublesInEuro(ruble, course);

    }

    /**
     * Выводит рез-т перевода рублей в евро по заданному курсу
     *
     * @param ruble сумма в рублях
     * @param course заданный курс
     */
    private static void rublesInEuro(double ruble, double course) {
        System.out.println("Ваша сумма в рублях составляет " + String.format("%.2f", ruble / course) + " в евро ");
    }
}
