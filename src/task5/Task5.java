package task5;

/**
 * Класс вывода простых чисел в пределах от 2 до 100
 *
 * @author Andreeva V.A.
 */
public class Task5 {
    public static void main(String[] args) {
        System.out.println("Простые числа: ");
        for (int i = 2; i <= 100; i++) {
            boolean flag = true;
            for (int j = 2; j <= i / j; j++)
                if ((i % j) == 0) {
                    flag = false;
                    break;
                }
            if (flag)
                System.out.println(i);
        }
    }
}
