package task9;

import java.util.Scanner;

/**
 * Класс, который выводит является ли заданное число палиндромом или нет
 *
 * @author Andreeva V.A.
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число - ");
        int a = scanner.nextInt();
        String i = String.valueOf(a);
        boolean flag = false;
        isPalindrome(i, false);
        if (flag) System.out.println("Это палиндром");
        else System.out.println("Это не палиндром");
    }

    /**
     * Возвращает true, если число является палиндромом, иначе false
     *
     * @param i число
     */
    private static boolean isPalindrome(String i, boolean flag) {

        if (i.equals(new StringBuilder().append(i).reverse().toString())) {
            flag = true;
        }
        return flag;
    }
}
